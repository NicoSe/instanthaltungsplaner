package Logic;

import Gui.MainPanel;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Main {

    public static ArrayList<String> Files = null;
    public static ArrayList<String> SearchInRows = null;

    public static JFrame frame;

    public static ArrayList<Integer> timeSpan = new ArrayList();
    public static char mapSeperator;
    public static char colSeperator;
    public static char ignorSeperator;

    public static void main(String[]args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                frame = new JFrame("Instandhaltungsübersicht V1.0");
                JPanel gui = new MainPanel().getMainPanel();
                frame.setContentPane(gui);
                frame.pack();
                frame.setMinimumSize(new Dimension(900,700));
                frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        } );

    }
}