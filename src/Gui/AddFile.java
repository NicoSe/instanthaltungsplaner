package Gui;

import Logic.Main;
import org.apache.poi.util.StringUtil;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;

public class AddFile extends JDialog {
    private JPanel contentPane;
    private JButton neueDateiButton;
    private JButton loescheDateiButton;
    private JButton schliesenButton;
    private JList<Object> pathList;
    private JList<Object> columnList;
    private JButton speichernButton;
    private JButton spaltenAendernButton;
    private JButton tabelleLoeschenButton;
    private JButton importierePfadeButton;
    private JButton importiereSpaltenButton;

    private ArrayList<String> pathListModel = new ArrayList<>();
    private ArrayList<String> columnListModel = new ArrayList<>();

    private final String columnInfoText = "Gib für jede Mappe/Seite der Datei an, welche Spalte überprüft werden soll. " +
            "Wichtig alle Mappen müssen angegeben werden. Wenn eine Mappe nicht wichtig ist kann man diese ignorieren lassen." +
            "\n Beispiel: BE" + Main.mapSeperator + "R" + Main.colSeperator + "L" + Main.mapSeperator + Main.ignorSeperator +
            "\n In der ersten Mappe soll Spalte BE überprüft werden, in der Zweiten R unf L und die letzte Mappe soll ignoriert werden.";

    public AddFile() {
        setContentPane(contentPane);
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        setTitle("Dateimanager");
        
        getRootPane().setDefaultButton(neueDateiButton);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        // Fill lists based on informations from Main class or settings.txt file or initialize them if any of them has informations
        if (Main.Files == null && Main.SearchInRows == null){
            try {
                File source = new File("./Bin/SourceList.txt");
                File source2 = new File("./Bin/ColumnList.txt");
                if (source.exists() && source2.exists()) {
                    BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(source)));
                    String file1 = bf.readLine();
                    bf = new BufferedReader(new InputStreamReader(new FileInputStream(source2)));
                    String file2 = bf.readLine();
                    bf.close();
                    if (file1 == null && file2 == null) {
                        pathListModel.clear();
                        columnListModel.clear();
                        pathList.setListData(pathListModel.toArray());
                        columnList.setListData(columnListModel.toArray());
                    } else {
                        loadFromFile();
                    }
                }
                else {
                    new File("./Bin/SourceList.txt");
                    new File("./Bin/ColumnList.txt");
                    pathListModel.clear();
                    columnListModel.clear();
                    pathList.setListData(pathListModel.toArray());
                    columnList.setListData(columnListModel.toArray());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            pathListModel = Main.Files;
            columnListModel = Main.SearchInRows;
            pathList.setListData(pathListModel.toArray());
            columnList.setListData(columnListModel.toArray());
        }

        columnList.setSelectionModel(new NoSelectionModel());


        neueDateiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addFile();
            }
        });

        spaltenAendernButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeColumn();
            }
        });

        loescheDateiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteFile();
            }
        });

        tabelleLoeschenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int access = JOptionPane.showInternalConfirmDialog(null, "Wollen Sie die Tabelle wirklich löschen? Dies kann icht mehr rückgängig gemacht werden!", "Achtung",JOptionPane.YES_NO_OPTION);
                if (access == 0) {
                    pathListModel.clear();
                    columnListModel.clear();
                    pathList.setListData(pathListModel.toArray());
                    columnList.setListData(columnListModel.toArray());
                    Main.Files = pathListModel;
                    Main.SearchInRows = columnListModel;
                }
            }
        });

        speichernButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveLists();
            }
        });

        schliesenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveLists();
                dispose();
            }
        });

        importierePfadeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                importPaths();
                saveLists();
            }
        });

        importiereSpaltenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                importCols();
                saveLists();
            }
        });

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                saveLists();
                dispose();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveLists();
                dispose();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    // change the column settings of a selected row in pathlist
    private void changeColumn() {
        if (pathList.getSelectedIndex() < 0) return;
        String rowInfo = JOptionPane.showInputDialog(this, columnInfoText);
        while(!checkRowInfo(rowInfo)){
            rowInfo = JOptionPane.showInputDialog(this, columnInfoText, rowInfo);
        }
        columnListModel.set(pathList.getSelectedIndex(), rowInfo);
        columnList.setListData(columnListModel.toArray());
        columnList.updateUI();
    }

    // load al information from the source files
    public boolean loadFromFile() {
        File source = new File("./Bin/SourceList.txt");
        File source2 = new File("./Bin/ColumnList.txt");
        if (source.exists() && source2.exists()) {
            try {
                BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(source)));
                String line = null;
                while ((line = bf.readLine()) != null) {
                    pathListModel.add(line);
                }

                bf = new BufferedReader(new InputStreamReader(new FileInputStream(source2)));
                line = null;
                while ((line = bf.readLine()) != null) {
                    columnListModel.add(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            pathList.setListData(pathListModel.toArray());
            columnList.setListData(columnListModel.toArray());
            Main.Files = pathListModel;
            Main.SearchInRows = columnListModel;
            return true;
        }
        else {
            new File("./Bin/SourceList.txt");
            new File("./Bin/ColumnList.txt");
            pathListModel.clear();
            columnListModel.clear();
            pathList.setListData(pathListModel.toArray());
            columnList.setListData(columnListModel.toArray());
            return false;
        }
    }

    // import own source file for path
    public boolean importPaths() {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Wähle SourceList.txt-Datei:");
        chooser.setCurrentDirectory(new File("C:/"));
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File source = chooser.getSelectedFile();
            if (source == null) return false;
            if (source.exists()) {
                try {
                    BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(source)));
                    String line = null;
                    while ((line = bf.readLine()) != null) {
                        pathListModel.add(line);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                pathList.setListData(pathListModel.toArray());
                Main.Files = pathListModel;
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    // import own source file for columns
    public boolean importCols() {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Wähle ColumnList.txt-Datei:");
        chooser.setCurrentDirectory(new File("C:/"));
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File source = chooser.getSelectedFile();
            if (source == null) return false;
            if (source.exists()) {
                try {
                    BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(source)));
                    String line = null;
                    while ((line = bf.readLine()) != null) {
                        columnListModel.add(line);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                columnList.setListData(columnListModel.toArray());
                Main.SearchInRows = columnListModel;
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    // open filechooser to select and add a file to the programm
    private void addFile(){
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Wähle xlsx oder xls Datei:");
        chooser.setCurrentDirectory(new File("C:/"));
        chooser.setAcceptAllFileFilterUsed(true);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File path = chooser.getSelectedFile();
            if (!(path.isFile() && path.canRead())){
                JOptionPane.showConfirmDialog(this, "Datei kann nicht geöffnet werden.");
                return;
            }
            pathListModel.add(path.getPath());
            pathList.setListData(pathListModel.toArray());
            pathList.updateUI();

            String rowInfo = JOptionPane.showInputDialog(this, columnInfoText);
            while(!checkRowInfo(rowInfo)){
                rowInfo = JOptionPane.showInputDialog(this, columnInfoText, rowInfo);
            }
            columnListModel.add(rowInfo);
            columnList.setListData(columnListModel.toArray());
            columnList.updateUI();
        }
    }

    // check if column information are correct, true if correct
    private boolean checkRowInfo(String rowInfo){
        if (rowInfo == null){
            return true;
        }
        rowInfo.trim();
        if (rowInfo.equals("")){
            return true;
        }
        return true;
        /*
        if (rowInfo.contains(String.valueOf(Main.mapSeperator))) {
            for (String s : rowInfo.split(String.valueOf(Main.mapSeperator))) {
                if (s.equals(String.valueOf(Main.ignorSeperator))) {
                } else if (s.contains(String.valueOf(Main.colSeperator))) {
                    for (String col : s.split(String.valueOf(Main.colSeperator))) {
                        if (col.matches("[A-Z]+")) {
                        } else {
                            return false;
                        }
                    }
                } else if (s.matches("[A-Z]+")) {
                } else {
                    return false;
                }
            }
        } else {
            if (rowInfo.equals(String.valueOf(Main.ignorSeperator)) || rowInfo.matches("[A-Z]+")) {
                return true;
            } else if (rowInfo.contains(String.valueOf(Main.colSeperator))){
                for (String col : rowInfo.split(String.valueOf(Main.colSeperator))) {
                    if (col.matches("[A-Z]+")) {
                    } else {
                        return false;
                    }
                }
            }else{
                return false;
            }
        }
        return false;*/
    }

    // delete a selected file from the list
    private void deleteFile() {
        columnListModel.remove(pathList.getSelectedIndex());
        columnList.setListData(columnListModel.toArray());

        pathListModel.remove(pathList.getSelectedIndex());
        pathList.setListData(pathListModel.toArray());

        pathList.clearSelection();
        pathList.updateUI();
        columnList.updateUI();
    }

    // save the lists in the files
    private void saveLists(){
        Main.Files = pathListModel;
        Main.SearchInRows = columnListModel;

        File settings = new File("./Bin/");
        if (!settings.isDirectory()){
            settings.mkdir();
        }

        File source = new File("./Bin/SourceList.txt");
        File source2 = new File("./Bin/ColumnList.txt");

        try {
            PrintWriter pWriter = new PrintWriter(new BufferedWriter(new FileWriter(source)));
            for(int i =0; i < pathListModel.size(); i++){
                pWriter.println(pathListModel.get(i));
            }
            pWriter.close();

            PrintWriter pWriter2 = new PrintWriter(new BufferedWriter(new FileWriter(source2)));
            for(int i =0; i < columnListModel.size(); i++){
                pWriter2.println(columnListModel.get(i));
            }
            pWriter2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dispose();
    }

    // noselectionmodel to avoid to select something in the columnlist
    private static class NoSelectionModel extends DefaultListSelectionModel {

        @Override
        public void setAnchorSelectionIndex(final int anchorIndex) {}

        @Override
        public void setLeadAnchorNotificationEnabled(final boolean flag) {}

        @Override
        public void setLeadSelectionIndex(final int leadIndex) {}

        @Override
        public void setSelectionInterval(final int index0, final int index1) { }
    }
}
