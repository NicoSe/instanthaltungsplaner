package Gui;

import Logic.Main;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.*;


public class MainPanel {
    private JPanel Panel1;
    private JButton generiereTabelleButton;
    private JButton loescheTabelleButton;
    private JTabbedPane tabbedPane1;
    private JTable table1;
    private JTable table2;
    private JTable table3;
    private JButton spalteOeffnenButton;
    private JMenuBar menubar;

    private ArrayList<ArrayList<String>> tab1 = new ArrayList<>();
    private ArrayList<ArrayList<String>> tab2 = new ArrayList<>();
    private ArrayList<ArrayList<String>> tab3 = new ArrayList<>();

    // Basic initialization of onclick and options and menubar
    public MainPanel(){

        // Menu settings
        JMenuBar menubar = new JMenuBar();
        JMenu dateimenu = new JMenu("Dateiliste");
        JMenu optionmenu = new JMenu("Optionen");
        menubar.add(dateimenu);
        menubar.add(optionmenu);
        Main.frame.setJMenuBar(menubar);

        tabbedPane1.removeTabAt(0);
        tabbedPane1.removeTabAt(0);
        tabbedPane1.removeTabAt(0);
        tabbedPane1.addTab("Zeitraum 1", new JScrollPane(table1));
        tabbedPane1.addTab("Zeitraum 2", new JScrollPane(table2));
        tabbedPane1.addTab("Zeitraum 3", new JScrollPane(table3));

        table1.setDefaultEditor(Object.class, null);
        table2.setDefaultEditor(Object.class, null);
        table3.setDefaultEditor(Object.class, null);

        File settings = new File("./Bin/");
        if (!settings.isDirectory()){
            settings.mkdir();
        }

        loadSettings();

        dateimenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        AddFile dialog = new AddFile();
                        dialog.pack();
                        dialog.setVisible(true);
                    }
                });
            }
        });

        optionmenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Options dialog = new Options(tabbedPane1, table2, table3);
                        dialog.pack();
                        dialog.setVisible(true);
                    }
                });
            }
        });

        generiereTabelleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread generateThread = new Thread(() -> {
                    generateTable();
                });
                generateThread.start();
            }
        });

        loescheTabelleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread delThread = new Thread(() -> {
                    deleteTable();
                });
                delThread.start();

            }
        });

        spalteOeffnenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table;
                if (tabbedPane1.getSelectedIndex() == 0) table = table1;
                else if (tabbedPane1.getSelectedIndex() == 1) table = table2;
                else if (tabbedPane1.getSelectedIndex() == 2) table = table3;
                else return;

                int row = table.getSelectedRow();
                if (row == -1) return;
                String path = String.valueOf(table.getModel().getValueAt(row, 4));
                String sheet = String.valueOf(table.getModel().getValueAt(row, 1));

                String command = "start excel \"" + path + "\" /e/" + sheet;
                command = command.replace("\\", "\\\\");
                try {
                    Process pro = Runtime.getRuntime().exec(command);
                    if ( pro != null){
                        pro.waitFor();
                    }
                } catch (Exception ignored){}
            }
        });
    }

    // return main panel to open the gui
    public JPanel getMainPanel(){
        return Panel1;
    }

    // function to clear the tables and the defaulttablemodel and data
    private void deleteTable() {
        tab1.clear();
        tab2.clear();
        tab3.clear();

        String [] heads = {"Datei", "Mappe", "Zeile;Spalte", "Läuft ab in", "Pfad"};

        table1.setModel(new DefaultTableModel(listToStringList(tab1), heads));
        table2.setModel(new DefaultTableModel(listToStringList(tab2), heads));
        table3.setModel(new DefaultTableModel(listToStringList(tab3), heads));
    }

    // generates the content of the tables
    private void generateTable() {
        loadSource();
        fillTables();
    }

    // load settings from optins and source files
    private void loadSettings(){
        File settings = new File("./Bin/Settings.txt");

        try {
            Main.timeSpan.clear();
            BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(settings)));
            String line = bf.readLine();
            line = bf.readLine();
            if (line.equals("true")) {
                line = bf.readLine();
                switch (line) {
                    case "7": {
                        Main.timeSpan.add(7);
                        break;
                    }
                    case "14": {
                        Main.timeSpan.add(14);
                        break;
                    }
                    case "21": {
                        Main.timeSpan.add(21);
                        break;
                    }
                    case "31": {
                        Main.timeSpan.add(31);
                        break;
                    }
                }
            }
            else if (line.equals("false")){
                bf.readLine();
                return;
            }

            int temp = 0;
            bf.readLine();
            line = bf.readLine();
            if (line.equals("true")) {
                line = bf.readLine();
                switch (line) {
                    case "7": {
                        Main.timeSpan.add(7);
                        break;
                    }
                    case "14": {
                        Main.timeSpan.add(14);
                        break;
                    }
                    case "21": {
                        Main.timeSpan.add(21);
                        break;
                    }
                    case "31": {
                        Main.timeSpan.add(31);
                        break;
                    }
                }
            }
            else if (line.equals("false")){
                tabbedPane1.removeTabAt(1);
                temp++;
                bf.readLine();
            }

            bf.readLine();
            line = bf.readLine();
            if (line.equals("true")) {
                if (temp == 1) tabbedPane1.setTitleAt(1, "Zeitraum 2");
                line = bf.readLine();
                switch (line) {
                    case "7": {
                        Main.timeSpan.add(7);
                        break;
                    }
                    case "14": {
                        Main.timeSpan.add(14);
                        break;
                    }
                    case "21": {
                        Main.timeSpan.add(21);
                        break;
                    }
                    case "31": {
                        Main.timeSpan.add(31);
                        break;
                    }
                }
            }
            else if (line.equals("false")){
                if (temp == 0) tabbedPane1.removeTabAt(2);
                else if (temp == 1) tabbedPane1.removeTabAt(1);
                bf.readLine();
            }

            Character [] seperator = {';',',','.',':','-','!','?','|','<','>','/','\\','$','@'};

            bf.readLine();
            line = bf.readLine();
            Main.mapSeperator = seperator[Integer.parseInt(line)];

            bf.readLine();
            line = bf.readLine();
            Main.colSeperator = seperator[Integer.parseInt(line)];

            bf.readLine();
            line = bf.readLine();
            Main.ignorSeperator = seperator[Integer.parseInt(line)];

            bf.close();
        } catch (Exception ignored) {
        }

    }

    // use sorce files to loop over all files and sheets und columns
    private void loadSource(){
        File source = new File("./Bin/SourceList.txt");
        File column = new File("./Bin/ColumnList.txt");
        if (source.exists() && column.exists()){
            try {
                BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(source)));
                BufferedReader bf2 = new BufferedReader(new InputStreamReader(new FileInputStream(column)));
                String line, line2;

                while ((line=bf.readLine()) != null && (line2=bf2.readLine()) != null){     // loop for all files
                    ArrayList<ArrayList<Integer>> neededColumns = analyzeColumns(line2);

                    // get workbook by fileending
                    FileInputStream exelFile = new FileInputStream(new File(line));
                    String[] end = line.split("\\.");
                    String fileend = end[end.length-1];
                    Workbook workbook;
                    switch (fileend) {
                        case "xlsx":
                            workbook = new XSSFWorkbook(exelFile);
                            break;
                        case "xls":
                            workbook = new HSSFWorkbook(exelFile);
                            break;
                        case "xml":
                            workbook = convertXMLtoXLSX(line);
                            if (workbook == null) {
                                JOptionPane.showMessageDialog(Panel1, "Die Datei " + line + " konnte nicht zu einer " +
                                                                      ".xlsx-datei konvertiert werden. Sie wird daher vom Programm nicht überprüft. " +
                                                                      "Bitte konvertieren Sie die datei über exel selbst.", "Achtung",
                                                              JOptionPane.WARNING_MESSAGE);
                                continue;
                            }
                            break;
                        default:
                            JOptionPane.showMessageDialog(Panel1, "Die Datei " + line + " wurde übersprungen, da sie " +
                                    "keinen aktzeptierten Dateityp besitzt!", "Achtung", JOptionPane.WARNING_MESSAGE);
                            continue;
                    }

                    Iterator<Sheet> sheetIterator = workbook.sheetIterator();
                    int sheetIndex = 0;

                    while (sheetIterator.hasNext()) {    // loop for all maps
                        Sheet currentSheet = sheetIterator.next();
                        Iterator<Row> rowIterator = currentSheet.iterator();
                        if (neededColumns.size() <= sheetIndex){
                            break;
                        }
                        if (neededColumns.get(sheetIndex).get(0) == -1){
                            sheetIndex++;
                            continue;
                        }


                        while (rowIterator.hasNext()) {     // loop for all rows
                            Row currentRow = rowIterator.next();
                            if (currentRow.getRowNum() == 0) continue;
                            try{
                                for (int col=0; col < neededColumns.get(sheetIndex).size(); col++) {
                                    Cell currentCell = currentRow.getCell(neededColumns.get(sheetIndex).get(col));
                                    if (currentCell.getRichStringCellValue().equals("")) continue;
                                    if (currentCell.getStringCellValue().equals("")) continue;
                                    if (currentCell.getCellType() == CellType.NUMERIC) {
                                        Date temp = currentCell.getDateCellValue();
                                        GregorianCalendar date = new GregorianCalendar();
                                        date.setTime(temp);
                                        GregorianCalendar today = new GregorianCalendar();
                                        long diff = today.getTimeInMillis() - date.getTimeInMillis();
                                        int ddiff = (int) (diff / (1000 * 60 * 60 * 24));
                                        ddiff = ddiff * -1;

                                        if (Main.timeSpan.size() == 1)
                                            if (ddiff < Main.timeSpan.get(0)) {
                                                addToTable(tab1, currentCell, line, ddiff);
                                            }
                                        if (Main.timeSpan.size() == 2) {
                                            if (ddiff < Main.timeSpan.get(0)) {
                                                addToTable(tab1, currentCell, line, ddiff);
                                            }
                                            if (Main.timeSpan.get(0) <= ddiff && ddiff < Main.timeSpan.get(1)) {
                                                addToTable(tab2, currentCell, line, ddiff);
                                            }
                                        }
                                        if (Main.timeSpan.size() == 3) {
                                            if (ddiff < Main.timeSpan.get(0)) {
                                                addToTable(tab1, currentCell, line, ddiff);
                                            }
                                            if (Main.timeSpan.get(0) <= ddiff && ddiff < Main.timeSpan.get(1)) {
                                                addToTable(tab2, currentCell, line, ddiff);
                                            }
                                            if (Main.timeSpan.get(1) <= ddiff && ddiff < Main.timeSpan.get(2)) {
                                                addToTable(tab3, currentCell, line, ddiff);
                                            }
                                        }
                                    } else if (currentCell.getCellType() == CellType.FORMULA) {
                                        GregorianCalendar date = getDatefromFormula(currentCell.getCellFormula(), currentSheet);

                                        if (date == null) {
                                            continue;
                                        }

                                        GregorianCalendar today = new GregorianCalendar();
                                        long diff = today.getTimeInMillis() - date.getTimeInMillis();
                                        int ddiff = (int) (diff / (1000 * 60 * 60 * 24));
                                        ddiff = ddiff * -1;

                                        if (Main.timeSpan.size() == 1)
                                            if (ddiff < Main.timeSpan.get(0)) {
                                                addToTable(tab1, currentCell, line, ddiff);
                                            }
                                        if (Main.timeSpan.size() == 2) {
                                            if (ddiff < Main.timeSpan.get(0)) {
                                                addToTable(tab1, currentCell, line, ddiff);
                                            }
                                            if (Main.timeSpan.get(0) <= ddiff && ddiff < Main.timeSpan.get(1)) {
                                                addToTable(tab2, currentCell, line, ddiff);
                                            }
                                        }
                                        if (Main.timeSpan.size() == 3) {
                                            if (ddiff < Main.timeSpan.get(0)) {
                                                addToTable(tab1, currentCell, line, ddiff);
                                            }
                                            if (Main.timeSpan.get(0) <= ddiff && ddiff < Main.timeSpan.get(1)) {
                                                addToTable(tab2, currentCell, line, ddiff);
                                            }
                                            if (Main.timeSpan.get(1) <= ddiff && ddiff < Main.timeSpan.get(2)) {
                                                addToTable(tab3, currentCell, line, ddiff);
                                            }
                                        }
                                    }
                                }
                            } catch (NullPointerException | IndexOutOfBoundsException ignored){
                            }
                        }
                        sheetIndex++;
                    }
                    workbook.close();
                    exelFile.close();
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    // use the formula of a cell (O2+20) to calculate the final date to be used in the table
    private GregorianCalendar getDatefromFormula(String i, Sheet sheet) {
        GregorianCalendar result = new GregorianCalendar();
        int addDays = 0;

        if (i.contains("+")){
            for (String a : i.split("\\+")){
                if (a.contains("*") || a.contains("/") || a.contains("-")){
                    addDays += (int) eval(a);
                }
                else if (a.matches("[0-9]+")) {
                    addDays += Integer.parseInt(a);
                }
                else {
                    Cell cell = getCellformAddress(a, sheet);
                    if (cell.getCellType() == CellType.NUMERIC){
                        Date temp = cell.getDateCellValue();
                        result = new GregorianCalendar();
                        result.setTime(temp);
                    }
                    else if (cell.getCellType() == CellType.FORMULA){
                        result = getDatefromFormula(cell.getCellFormula(), sheet);
                    }
                    else return null;

                }
            }
        }
        result.add(Calendar.DAY_OF_YEAR, addDays);
        return result;
    }

    // get a specific cell from an address (M20) of a sheet
    private Cell getCellformAddress(String address, Sheet sheet) {
        char[] ch = address.toCharArray();
        StringBuilder AAA = new StringBuilder();
        StringBuilder OOO = new StringBuilder();
        for (int j=0; j<address.length(); j++){
            String temp = String.valueOf(ch[j]);
            if (temp.matches("[A-Z]+")){
                AAA.append(temp);
            }
            if (temp.matches("[0-9]+")){
                OOO.append(temp);
            }
        }
        int row = charToIntCol(AAA.toString());
        int col = Integer.parseInt(OOO.toString());

        return sheet.getRow(col-1).getCell(row);
    }

    // add information of an entry to the defaulttablemodel, only if the entry must be shown
    private void addToTable(ArrayList tab, Cell cell, String path, int diff) {
        ArrayList<String> a = new ArrayList<>();
        String[] temp = path.split("\\\\");
        diff++;

        a.add(temp[temp.length-1]);
        a.add(cell.getRow().getSheet().getSheetName());
        a.add(cell.getAddress().toString());
        if (diff == 0) {
            a.add("heute");
        }
        else if (diff == 1){
            a.add("morgen");
        }
        else if (diff > 1){
            a.add("in " + diff + " Tagen");
        }
        else {
            a.add("bereits abgelaufen");
        }
        a.add(path);

        tab.add(a);
    }

    // fill all tables with the models and clear and size the table afterwards
    private void fillTables() {
        String [] heads = {"Datei", "Mappe", "Zeile;Spalte", "Läuft ab in", "Pfad"};

        table1.setModel(new DefaultTableModel(listToStringList(tab1), heads));
        table2.setModel(new DefaultTableModel(listToStringList(tab2), heads));
        table3.setModel(new DefaultTableModel(listToStringList(tab3), heads));

        tab1.clear();
        tab2.clear();
        tab3.clear();

        setTableColWidth();
    }

    // size all columns
    private void setTableColWidth(){
        table1.getColumnModel().getColumn(0).setMinWidth(200);
        table1.getColumnModel().getColumn(0).setMaxWidth(500);
        table1.getColumnModel().getColumn(1).setMinWidth(200);
        table1.getColumnModel().getColumn(1).setMaxWidth(350);
        table1.getColumnModel().getColumn(2).setMinWidth(50);
        table1.getColumnModel().getColumn(2).setMaxWidth(170);
        table1.getColumnModel().getColumn(3).setMinWidth(120);
        table1.getColumnModel().getColumn(3).setMaxWidth(250);
        table1.getColumnModel().getColumn(4).setMinWidth(400);
        table1.getColumnModel().getColumn(4).setMaxWidth(1000);

        table2.getColumnModel().getColumn(0).setMinWidth(200);
        table2.getColumnModel().getColumn(0).setMaxWidth(500);
        table2.getColumnModel().getColumn(1).setMinWidth(200);
        table2.getColumnModel().getColumn(1).setMaxWidth(350);
        table2.getColumnModel().getColumn(2).setMinWidth(50);
        table2.getColumnModel().getColumn(2).setMaxWidth(170);
        table2.getColumnModel().getColumn(3).setMinWidth(120);
        table2.getColumnModel().getColumn(3).setMaxWidth(250);
        table2.getColumnModel().getColumn(4).setMinWidth(400);
        table2.getColumnModel().getColumn(4).setMaxWidth(1000);

        table3.getColumnModel().getColumn(0).setMinWidth(200);
        table3.getColumnModel().getColumn(0).setMaxWidth(500);
        table3.getColumnModel().getColumn(1).setMinWidth(200);
        table3.getColumnModel().getColumn(1).setMaxWidth(350);
        table3.getColumnModel().getColumn(2).setMinWidth(50);
        table3.getColumnModel().getColumn(2).setMaxWidth(170);
        table3.getColumnModel().getColumn(3).setMinWidth(120);
        table3.getColumnModel().getColumn(3).setMaxWidth(250);
        table3.getColumnModel().getColumn(4).setMinWidth(400);
        table3.getColumnModel().getColumn(4).setMaxWidth(1000);
    }

    // convert a unlimited doubled arraylist to a limited string array
    private String[][] listToStringList(ArrayList<ArrayList<String>> list){
        String [][] result = new String [0][0];
        if (list.size() != 0) {
            result = new String[list.size()][list.get(0).size()];
            for (int i = 0; i < list.size(); i++) {
                for (int j=0; j<list.get(0).size(); j++){
                    result[i][j] = list.get(i).get(j);
                }
            }
        }
        return result;
    }

    // save all columns that are important as arraylist of arraylists, every line is a sheet with multiple columns that can be proved
    private ArrayList<ArrayList<Integer>> analyzeColumns(String line2) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        String[] map = line2.split(String.valueOf(Main.mapSeperator));
        for (String s : map) {
            ArrayList<Integer> temp = new ArrayList<>();
            if (s.contains(String.valueOf(Main.ignorSeperator))) {
                temp.add(-1);
                result.add(temp);
                continue;
            }
            String[] columns = s.split(String.valueOf(Main.colSeperator));

            if (columns.length == 1) {
                temp.add(charToIntCol(columns[0]));
                result.add(temp);
            } else {
                for (String column : columns) {
                    temp.add(charToIntCol(column));
                }
                result.add(temp);
            }
        }
        return result;
    }

    // translate excel chars to index (AA=27)
    private Integer charToIntCol(String column) {
        int number = 0;
        for (char l:column.toCharArray()){
            if (!(column.contains(String.valueOf(l)))){
                return -1;
            }
            number = number * 26 + ((int) l) - ((int) 'A') + 1;
        }
        return number - 1;
    }

    // evaluate a string with multiple operations
    private double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`
            //        | number | functionName factor | factor `^` factor

            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if      (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if      (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos);
                    x = parseFactor();
                    switch (func) {
                        case "sqrt":
                            x = Math.sqrt(x);
                            break;
                        case "sin":
                            x = Math.sin(Math.toRadians(x));
                            break;
                        case "cos":
                            x = Math.cos(Math.toRadians(x));
                            break;
                        case "tan":
                            x = Math.tan(Math.toRadians(x));
                            break;
                        default:
                            throw new RuntimeException("Unknown function: " + func);
                    }
                } else {
                    throw new RuntimeException("Unexpected: " + (char)ch);
                }

                if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }

    // convert a .xml file to a xlsx file to work with it
    private XSSFWorkbook convertXMLtoXLSX(String path){
        // read XML and convert into XLS (Excel) and save
        /*XLSDocument document = new XLSDocument();
        SimpleXMLConverter tools = new SimpleXMLConverter(document);
        tools.LoadXML("AdvancedReport.xml");
        File.Delete("AdvancedReportFromXML.xls");
        document.SaveAs("AdvancedReportFromXML.xls");
        document.Close();*/
        return null;
    }
}
