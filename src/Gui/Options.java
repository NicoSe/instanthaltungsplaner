package Gui;

import Logic.Main;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;

public class Options extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox Zeitraum1Combo;
    private JComboBox MapComboBox;
    private JComboBox ColumnComboBox;
    private JComboBox IgnorComboBox;
    private JButton standarteinstellungenButton;
    private JCheckBox Zeitraum1Check;
    private JCheckBox Zeitraum2Check;
    private JCheckBox Zeitraum3Check;
    private JComboBox Zeitraum2Combo;
    private JComboBox Zeitraum3Combo;

    private int[] timespan = {7,14,21,31};
    private JTable table2;
    private JTable table3;
    private JTabbedPane tabbedPane1;

    public Options(JTabbedPane tabbedPane1, JTable table2, JTable table3) {
        setContentPane(contentPane);
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        setTitle("Optionen");
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        this.tabbedPane1 = tabbedPane1;
        this.table2 = table2;
        this.table3 = table3;

        loadSettings();

        standarteinstellungenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });

        Zeitraum1Check.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (Zeitraum1Check.isSelected()){
                    Zeitraum1Combo.setEnabled(true);
                }
                else {
                    Zeitraum1Combo.setEnabled(false);
                }
            }
        });

        Zeitraum2Check.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (Zeitraum2Check.isSelected()){
                    Zeitraum2Combo.setEnabled(true);
                }
                else {
                    Zeitraum2Combo.setEnabled(false);
                }
            }
        });

        Zeitraum3Check.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (Zeitraum3Check.isSelected()){
                    Zeitraum3Combo.setEnabled(true);
                }
                else {
                    Zeitraum3Combo.setEnabled(false);
                }
            }
        });
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveSettings();
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    // save settings and send the to the main method
    private void onOK() {
        if (valid()) {
            setSeperators();
            saveSettings();
            if (Main.timeSpan.size() != 0) Main.timeSpan.clear();

            if (Zeitraum1Check.isSelected()) Main.timeSpan.add(timespan[Zeitraum1Combo.getSelectedIndex()]);
            if (Zeitraum2Check.isSelected()) Main.timeSpan.add(timespan[Zeitraum2Combo.getSelectedIndex()]);
            if (Zeitraum3Check.isSelected()) Main.timeSpan.add(timespan[Zeitraum3Combo.getSelectedIndex()]);

            Main.mapSeperator = MapComboBox.getItemAt(MapComboBox.getSelectedIndex()).toString().charAt(0);
            Main.colSeperator = ColumnComboBox.getItemAt(ColumnComboBox.getSelectedIndex()).toString().charAt(0);
            Main.ignorSeperator = IgnorComboBox.getItemAt(IgnorComboBox.getSelectedIndex()).toString().charAt(0);

            dispose();
        }
    }

    // set the seperators in the columnList.txt based on the owners choice
    private void setSeperators() {
        try {
            // get previous settings
            BufferedReader bs = new BufferedReader(new InputStreamReader(new FileInputStream(new File("./Bin/Settings.txt"))));
            bs.readLine(); bs.readLine(); bs.readLine(); bs.readLine();
            bs.readLine(); bs.readLine(); bs.readLine(); bs.readLine();
            bs.readLine(); bs.readLine();
            String ms = bs.readLine();
            bs.readLine();
            String cs = bs.readLine();
            bs.readLine();
            String is = bs.readLine();
            bs.close();

            // override with new settings
            BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(new File("./Bin/ColumnList.txt"))));
            String line;
            ArrayList<String> newFile = new ArrayList<>();
            while ((line=bf.readLine()) != null){
                line =line.replace(String.valueOf(MapComboBox.getItemAt(Integer.parseInt(ms))), String.valueOf(MapComboBox.getItemAt(MapComboBox.getSelectedIndex())));
                line = line.replace(String.valueOf(ColumnComboBox.getItemAt(Integer.parseInt(cs))), String.valueOf(ColumnComboBox.getItemAt(ColumnComboBox.getSelectedIndex())));
                line = line.replace(String.valueOf(IgnorComboBox.getItemAt(Integer.parseInt(is))), String.valueOf(IgnorComboBox.getItemAt(IgnorComboBox.getSelectedIndex())));
                newFile.add(line);
            }

            // save new file
            PrintWriter pWriter = new PrintWriter(new BufferedWriter(new FileWriter(new File("./Bin/ColumnList.txt"))));
            for(int i =0; i < newFile.size(); i++){
                pWriter.println(newFile.get(i));
            }
            pWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // check if all settings are valid, by order, asc, replication
    private boolean valid() {
        int det = 0; //change detection;

        // check the order of selected spans
        if (Zeitraum1Check.isSelected() == false){
            if (Zeitraum2Check.isSelected() == true){
                Zeitraum1Check.setSelected(true);
                Zeitraum1Combo.setSelectedIndex(Zeitraum2Combo.getSelectedIndex());
                det++;
                if (Zeitraum3Check.isSelected() == true){
                    Zeitraum2Combo.setSelectedIndex(Zeitraum3Combo.getSelectedIndex());
                    Zeitraum3Check.setSelected(false);
                    det++;
                }
                else {
                    Zeitraum2Check.setSelected(false);
                    det++;
                }
            }
            if (Zeitraum3Check.isSelected() == true){
                Zeitraum1Check.setSelected(true);
                Zeitraum1Combo.setSelectedIndex(Zeitraum3Combo.getSelectedIndex());
                Zeitraum3Check.setSelected(false);
                det++;
            }
        }
        else {
            if (Zeitraum2Check.isSelected() == false){
                if (Zeitraum3Check.isSelected() == true){
                    Zeitraum2Check.setSelected(true);
                    Zeitraum2Combo.setSelectedIndex(Zeitraum3Combo.getSelectedIndex());
                    Zeitraum3Check.setSelected(false);
                    det++;
                }
            }
        }

        // check order of span size
        if (Zeitraum1Check.isSelected() && Zeitraum2Check.isSelected() && Zeitraum3Check.isSelected()) {
            if (Zeitraum1Combo.getSelectedIndex() > Zeitraum2Combo.getSelectedIndex()) {
                int temp = Zeitraum1Combo.getSelectedIndex();
                Zeitraum1Combo.setSelectedIndex(Zeitraum2Combo.getSelectedIndex());
                Zeitraum1Combo.setSelectedIndex(temp);
                det++;
            }
            if (Zeitraum1Combo.getSelectedIndex() > Zeitraum3Combo.getSelectedIndex()) {
                int temp = Zeitraum1Combo.getSelectedIndex();
                Zeitraum1Combo.setSelectedIndex(Zeitraum3Combo.getSelectedIndex());
                Zeitraum3Combo.setSelectedIndex(temp);
                det++;
            }
            if (Zeitraum1Combo.getSelectedIndex() > Zeitraum2Combo.getSelectedIndex()) {
                int temp = Zeitraum1Combo.getSelectedIndex();
                Zeitraum1Combo.setSelectedIndex(Zeitraum2Combo.getSelectedIndex());
                Zeitraum1Combo.setSelectedIndex(temp);
                det++;
            }
            if (Zeitraum2Combo.getSelectedIndex() > Zeitraum3Combo.getSelectedIndex()) {
                int temp = Zeitraum2Combo.getSelectedIndex();
                Zeitraum2Combo.setSelectedIndex(Zeitraum3Combo.getSelectedIndex());
                Zeitraum3Combo.setSelectedIndex(temp);
                det++;
            }

            // check if some spans have the same size
            if (Zeitraum1Combo.getSelectedIndex() == Zeitraum2Combo.getSelectedIndex()){
                Zeitraum2Combo.setSelectedIndex(Zeitraum3Combo.getSelectedIndex());
                Zeitraum3Check.setSelected(false);
                det++;
            }
            else {
                if (Zeitraum1Combo.getSelectedIndex() == Zeitraum3Combo.getSelectedIndex()){
                    Zeitraum3Check.setSelected(false);
                    det++;
                }
            }
            if (Zeitraum1Combo.getSelectedIndex() == Zeitraum3Combo.getSelectedIndex()){
                Zeitraum3Check.setSelected(false);
                det++;
            }
        }
        else if (Zeitraum1Check.isSelected() && Zeitraum2Check.isSelected()){
            if (Zeitraum1Combo.getSelectedIndex() > Zeitraum2Combo.getSelectedIndex()) {
                int temp = Zeitraum1Combo.getSelectedIndex();
                Zeitraum1Combo.setSelectedIndex(Zeitraum2Combo.getSelectedIndex());
                Zeitraum2Combo.setSelectedIndex(temp);
                det++;
            }

            // check if some spans have the same size
            if (Zeitraum1Combo.getSelectedIndex() == Zeitraum2Combo.getSelectedIndex()){
                Zeitraum2Check.setSelected(false);
                det++;
            }
        }

        // refresh tabs
        try {
            tabbedPane1.removeTabAt(1);
            tabbedPane1.removeTabAt(1);
        } catch (Exception e){}

        if (Zeitraum2Check.isSelected()){
            tabbedPane1.addTab("Zeitraum 2", new JScrollPane(table2));
        }
        if (Zeitraum3Check.isSelected()){
            tabbedPane1.addTab("Zeitraum 3", new JScrollPane(table3));
        }

        if (det > 0) showMessage();

        // check if seperators are different
        if(MapComboBox.getSelectedIndex() == ColumnComboBox.getSelectedIndex() || MapComboBox.getSelectedIndex() ==
                IgnorComboBox.getSelectedIndex() || ColumnComboBox.getSelectedIndex() == IgnorComboBox.getSelectedIndex()){
            JOptionPane.showMessageDialog(null,
                    "Die ausgewählten Seperatoren beinhalten eine Überschneidung! Bitte überprüfen und änderen " +
                            "Sie diese ab!", "Warnung!", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }

    // show warning message, that appear if the calid method must change some settings
    private void showMessage(){
        JOptionPane.showMessageDialog(null,
                "Auf Grund Ihrer Auswahl wurden folgende Einstellungen verändert. " +
                        "\n- Bei Erkennung einer doppelten Zeitspanne wurde diese entfernt. " +
                        "\n- Die Zeitspannen wurden nach Größe sortiert." +
                        "\n- Lücken wurden geschlossen." +
                        "\n- Bei Änderung der Seperatoren wurde die nach ihren Einstellungen die Spalteneingabe verändert.",
                "Eine Nachricht",
                JOptionPane.WARNING_MESSAGE);
    }

    // reset all options to deafult
    private void reset(){
        Zeitraum1Check.setSelected(true);
        Zeitraum1Combo.setSelectedIndex(0);
        Zeitraum2Check.setSelected(false);
        Zeitraum2Combo.setSelectedIndex(1);
        Zeitraum2Combo.setEnabled(false);
        Zeitraum3Check.setSelected(false);
        Zeitraum3Combo.setSelectedIndex(2);
        Zeitraum3Combo.setEnabled(false);

        MapComboBox.setSelectedIndex(0);
        ColumnComboBox.setSelectedIndex(1);
        IgnorComboBox.setSelectedIndex(4);


        Main.mapSeperator = MapComboBox.getItemAt(MapComboBox.getSelectedIndex()).toString().charAt(0);
        Main.colSeperator = ColumnComboBox.getItemAt(ColumnComboBox.getSelectedIndex()).toString().charAt(0);
        Main.ignorSeperator = IgnorComboBox.getItemAt(IgnorComboBox.getSelectedIndex()).toString().charAt(0);
    }

    // save settings to file
    private void saveSettings() {

        File settings = new File("./Bin/");
        if (!settings.isDirectory()){
            settings.mkdir();
        }

        settings = new File("./Bin/Settings.txt");

        try {
            PrintWriter pWriter = new PrintWriter(new BufferedWriter(new FileWriter(settings)));
            pWriter.println("Span1:");
            pWriter.println(Zeitraum1Check.isSelected());
            pWriter.println(timespan[Zeitraum1Combo.getSelectedIndex()]);
            pWriter.println("Span2:");
            pWriter.println(Zeitraum2Check.isSelected());
            pWriter.println(timespan[Zeitraum2Combo.getSelectedIndex()]);
            pWriter.println("Span3:");
            pWriter.println(Zeitraum3Check.isSelected());
            pWriter.println(timespan[Zeitraum3Combo.getSelectedIndex()]);
            pWriter.println("Map seperator:");
            pWriter.println(MapComboBox.getSelectedIndex());
            pWriter.println("Column seperator:");
            pWriter.println(ColumnComboBox.getSelectedIndex());
            pWriter.println("Ignor indicator:");
            pWriter.println(IgnorComboBox.getSelectedIndex());
            pWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // load settings from file
    private void loadSettings(){
        File settings = new File("./Bin/Settings.txt");

        try {
            BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(settings)));
            String line = bf.readLine();
            line = bf.readLine();
            if (line.equals("true")) {
                Zeitraum1Check.setSelected(true);
                Zeitraum1Combo.setEnabled(true);
                line = bf.readLine();
                switch (line) {
                    case "7": {
                        Zeitraum1Combo.setSelectedIndex(0);
                        break;
                    }
                    case "14": {
                        Zeitraum1Combo.setSelectedIndex(1);
                        break;
                    }
                    case "21": {
                        Zeitraum1Combo.setSelectedIndex(2);
                        break;
                    }
                    case "31": {
                        Zeitraum1Combo.setSelectedIndex(3);
                        break;
                    }
                }
            }
            else if (line.equals("false")){
                Zeitraum1Check.setSelected(false);
                Zeitraum1Combo.setEnabled(false);
                bf.readLine();
            }

            bf.readLine();
            line = bf.readLine();
            if (line.equals("true")) {
                Zeitraum2Check.setSelected(true);
                Zeitraum2Combo.setEnabled(true);
                line = bf.readLine();
                switch (line) {
                    case "7": {
                        Zeitraum2Combo.setSelectedIndex(0);
                        break;
                    }
                    case "14": {
                        Zeitraum2Combo.setSelectedIndex(1);
                        break;
                    }
                    case "21": {
                        Zeitraum2Combo.setSelectedIndex(2);
                        break;
                    }
                    case "31": {
                        Zeitraum2Combo.setSelectedIndex(3);
                        break;
                    }
                }
            }
            else if (line.equals("false")){
                Zeitraum2Check.setSelected(false);
                Zeitraum2Combo.setEnabled(false);
                bf.readLine();
            }

            bf.readLine();
            line = bf.readLine();
            if (line.equals("true")) {
                Zeitraum3Check.setSelected(true);
                Zeitraum3Combo.setEnabled(true);
                line = bf.readLine();
                switch (line) {
                    case "7": {
                        Zeitraum3Combo.setSelectedIndex(0);
                        break;
                    }
                    case "14": {
                        Zeitraum3Combo.setSelectedIndex(1);
                        break;
                    }
                    case "21": {
                        Zeitraum3Combo.setSelectedIndex(2);
                        break;
                    }
                    case "31": {
                        Zeitraum3Combo.setSelectedIndex(3);
                        break;
                    }
                }
            }
            else if (line.equals("false")){
                Zeitraum3Check.setSelected(false);
                Zeitraum3Combo.setEnabled(false);
                bf.readLine();
            }

            bf.readLine();
            line = bf.readLine();
            MapComboBox.setSelectedIndex(Integer.parseInt(line));
            Main.mapSeperator = MapComboBox.getItemAt(MapComboBox.getSelectedIndex()).toString().charAt(0);

            bf.readLine();
            line = bf.readLine();
            ColumnComboBox.setSelectedIndex(Integer.parseInt(line));
            Main.colSeperator = ColumnComboBox.getItemAt(ColumnComboBox.getSelectedIndex()).toString().charAt(0);

            bf.readLine();
            line = bf.readLine();
            IgnorComboBox.setSelectedIndex(Integer.parseInt(line));
            Main.ignorSeperator = IgnorComboBox.getItemAt(IgnorComboBox.getSelectedIndex()).toString().charAt(0);

            bf.close();
        } catch (Exception e) {
            reset();
            return;
        }
    }
}
